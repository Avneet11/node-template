"use strict";
/*
 * This file contails all the routes that are related to
 * credit scoring of the user.
 */
const express = require("express");
const router = express.Router();
const auth = require("../../lib/auth");
const validate = require("express-validation");
const Validation = require("../user/validation");
const Controller = require("./vendorController");
const logger = require("../../utils/logger");

router.post("/vendor", (req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.addNew(req.body).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

router.get("/vendor" , auth.common,(req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.list(req.query).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

router.put("/vendor/login", (req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.login(req.body).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

module.exports = router;
