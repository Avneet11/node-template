"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true
    },
    coordinates: {
        type: [Number],
        required: true
    }
});

const VendorSchema = new Schema({
    email: { type: String, unique: true },
    password: { type: String, required: true },
    phoneNo: { type: Number, required: true },
    name: { type: String, required: true },
    gas: { type: Boolean, default: true },
    diesel: { type: Boolean, default: true },
    petrol: { type: Boolean, default: true },
    availabilty: { type: Boolean, default: true },
    location: pointSchema,
    token: { type: String },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

VendorSchema.index({ location: "2dsphere" })

module.exports = mongoose.model("vendor", VendorSchema);
