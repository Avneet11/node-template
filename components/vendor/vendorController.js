"use strict";

const jwt = require("jsonwebtoken");
const vendor = require("./vendor");
const bcrypt = require("bcrypt");
const _vendor = {};

_vendor.addNew = async (payloadData) => {
    let hash = await bcrypt.hash(
        payloadData.password,
        parseInt(process.env.SALT_ROUNDS)
    );
    payloadData.password = hash;
    payloadData.location = { type: "Point", coordinates: [payloadData.longitude, payloadData.latitude] }
    console.log("pay================",payloadData)
    let data = await new vendor(payloadData).save();
    if (!data) {
        throw new Error("vendor not created !!");
    } else {
        return {
            success: true,
            message: "Successfully Created",
            phoneNo: data.phoneNo,
            location: data.location.coordinates,
            id: data._id
        }
    }
};

_vendor.list = async (queryData) => {
    let criteria = {
        location:
        {
            $near:
            {
                $geometry: { type: "Point", coordinates: [queryData.longitude, queryData.latitude] },
                $maxDistance: 5000
            }
        }
    } 
    let data = await vendor.find(criteria);
    if (!data) {
        throw new Error("No vendor Found!!");
    } else {
        return {
            success: true,
            data: data
        }
    }
};

_vendor.login = async (payloadData) => {
    if (!payloadData.email || !payloadData.password) {
        throw new Error("please send email or password")
    } else {
        //Hash password
        const criteria = {
            email: payloadData.email
        }
        let vendorData = await vendor.findOne(criteria);
        if (!vendorData) {
            throw new Error("No vendor Found")
        }
        else {
            let pwPresent = await bcrypt.compare(payloadData.password, vendorData.password);
            if (pwPresent === true) {
                let token_Data = {
                    email: vendorData.email,
                    _id: vendorData._id
                };
                let token = jwt.sign(token_Data, process.env.SECRET, {
                    expiresIn: "1d" // expires in 24 hours
                });
                return {
                    success: true,
                    message: "Login successful",
                    token: token,
                    id: vendorData._id
                }
            }
            else {
                throw new Error("Password not matched")
            }
        }
    }
};

module.exports = _vendor;
