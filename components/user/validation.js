const Joi = require("joi");
const strictChecking = {
    allowUnknownBody: false,
    allowUnknownHeaders: true,
    allowUnknownQuery: false,
    allowUnknownParams: false,
    allowUnknownCookies: false
};

const register = {
    options: strictChecking,
    body: {
        email: Joi.string().required(),
        name: Joi.string().required(),
        password: Joi.string().required(),
        phoneNo: Joi.number().required(),
    }
};


module.exports = {
    register
};
