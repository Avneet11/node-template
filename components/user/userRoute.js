"use strict";
/*
 * This file contails all the routes that are related to
 * credit scoring of the user.
 */
const express = require("express");
const router = express.Router();
const auth = require("../../lib/auth");
const validate = require("express-validation");
const Validation = require("./validation");
const Controller = require("./userController");
const logger = require("../../utils/logger");

router.post("/createUser", validate(Validation.register), (req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.register(req.body).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

router.put("/user/login", (req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.login(req.body).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

router.put("/uploadUserImg", auth.common, 
    Controller.uploadUserImg
);

module.exports = router;
