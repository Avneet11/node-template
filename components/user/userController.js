"use strict";

const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const user = require("./user");
const fs = require('fs');
const multer = require('multer');
const dir = './uploads';
const bcrypt = require("bcrypt");
const _user = {};


_user.register = async (payloadData) => {
    let hash = await bcrypt.hash(
        payloadData.password,
        parseInt(process.env.SALT_ROUNDS)
    );
    payloadData.password = hash;
    let userData = await new user(payloadData).save();
    if (!userData) {
        throw new Error("User not created !!");
    } else {
        delete userData.password
        return {
            success: true,
            message: "Successfully Created",
            name: userData.name,
            phoneNo: userData.phoneNo,
            id: userData._id
        }
    }
};

_user.login = async (payloadData) => {
    if (!payloadData.email || !payloadData.password) {
        throw new Error("please send email or password")
    } else {
        //Hash password
        const criteria = {
            email: payloadData.email
        }
        let userData = await user.findOne(criteria);
        if (!userData) {
            throw new Error("No user Found")
        }
        else {
            let pwPresent = await bcrypt.compare(payloadData.password, userData.password);
        if (pwPresent === true) {
            let token_Data = {
                email: userData.email,
                _id: userData._id
            };
            let token = jwt.sign(token_Data, process.env.SECRET, {
                expiresIn: "1d" // expires in 24 hours
            }); 
            return {
                success: true,
                message: "Login successful",
                token: token,
                id: userData._id
            }
        }
        else {
            throw new Error("Password not matched") 
            }
        }
    }
};


/* function for upload user image*/
_user.uploadUserImg = async (req, res) => {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    upload(req, res, async err => {
        if (err) {
            res.json({
                success: false,
                message: err
            });
        } else {
            let criteria = {
                _id: mongoose.Types.ObjectId(req.user_id)
            };
            let profileImg =""
            if (req.file) {
                 profileImg = process.env.SERVER_URL + '/' + req.file.path;
            }
            let dataToAdd={
                profileImg: profileImg
            }
            let userData = await user.findOneAndUpdate(criteria, dataToAdd);
            if (!userData) {
                res.json({
                    message: "Something went wrong"
                }) 
            }
            else {
                res.json({
                    success: true,
                    message: "Image uploaded successfully"
                })
            }
        }
    });
}

const storage = multer.diskStorage({
    /* destination*/
    destination: function (req, file, cb) {
        cb(null, dir);
    },
    filename: function (req, file, cb) {
        cb(null, new Date().getTime().toString() + '-' + file.originalname);
    }
});
const upload = multer({ storage: storage, limits: { fileSize: 3110000 }}).single('Image');

module.exports = _user;
