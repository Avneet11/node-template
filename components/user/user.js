"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    email: { type: String, unique: true },
    password: { type: String, required: true },
    phoneNo: { type: Number, required: true },
    name: { type: String, required: true },
    status: { type: String, enum: ["active", "deleted","blocked"], default: "active" },
    profileImg: { type: String},
    token: { type: String },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("user", UserSchema);
