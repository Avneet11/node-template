"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var BookingSchema = new Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },
    vendorId: { type: mongoose.Schema.Types.ObjectId, ref: 'vendor', required: true },
    filingType: { type: String, enum: ["gas", "diesel", "petrol"], required: true },
    bookingStatus: { type: String,default:"pending" },
    vehicleDetails: { type: Array },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("booking", BookingSchema);
