"use strict";
/*
 * This file contails all the routes that are related to
 * credit scoring of the user.
 */
const express = require("express");
const router = express.Router();
const auth = require("../../lib/auth");
const Controller = require("./bookingController");
const logger = require("../../utils/logger");

router.post("/booking", auth.common, (req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.addNew(req.body, req.user_id).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

router.get("/booking", auth.common, (req, res) => {
    // send only the data that is required by the controller
    logger.info(req.body);
    Controller.list(req.query, req.user_id).then(response => {
        logger.info(response);
        if (!response) {
            res.status(500).send("Something went wrong");
        } else {
            res.status(200).json(response);
        }
    }).catch(error => {
        logger.error(error);
        res.status(403).json({
            message: error.message
        });
    });
});

module.exports = router;
