"use strict";

const booking = require("./booking");
const mongoose = require('mongoose');
const _booking = {};

_booking.addNew = async (payloadData,userId) => {
    payloadData.vendorId = payloadData.vendorId;
    payloadData.userId = userId;
    console.log("p======================",payloadData)
    let data = await new booking(payloadData).save();
    if (!data) {
        throw new Error("Booking not created !!");
    } else {
        return {
            success: true,
            message: "Booking Successfully Created",
            userId: data.userId,
            filingType: data.filingType,
            bookingId: data._id
        }
    }
};

_booking.list = async (payloadData, vendorId) => {
    let criteria = {
        vendorId: mongoose.Types.ObjectId(vendorId)
    }
    let data = await booking.find(criteria).populate({
        path: 'userId',
        match: { status: "active"}});
    if (!data) {
        throw new Error("booking not created !!");
    } else {
        return {
            success: true,
            data: data
        }
    }
};

module.exports = _booking;
