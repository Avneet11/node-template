"use strict";
/*
 * This file will have all controller functions
 */
const mongoose = require("mongoose");
const moment = require("moment");
const utils = require("../../utils");
const collections = require("../../config/mongoDriver");
const _visitor = require("../visitor/visitorController");
const _invite = require("../invite/inviteController");
const _dashboard = {};

_dashboard.count = async (queryData) => {
    let dateRange = {
        from: queryData.from,
        to: queryData.to
    }
    const visitor = await _visitor.getVisitors(dateRange)
    if (!visitor) {
        throw (new Error("Visitor not Found"))
    } else {
        const visitorCheckedIn = await collections.visitors.find({
            checkOut: {
                $eq: ""
            }
        }).toArray()
        if (!visitorCheckedIn) {
            throw (new Error("Visitor not Found"))
        } else {
            var start = new Date(queryData.from);
            var end = new Date(queryData.to);
            const visitorCheckedArray = await collections.visitors.aggregate([{
                $addFields: {
                    date: {
                        $dateFromString: {
                            dateString: "$checkIn"
                        }
                    }
                }
            }, {
                $match: {
                    date: {
                        $gte: start,
                        $lte: end
                    }
                }
            },
            {
                $addFields: {
                    dateString: {
                        $dateToString: {
                            format: "%Y-%m-%dT%H:%M:%S",
                            date: "$date"
                        }
                    }
                }
            },
            {
                $group: {
                    _id: "$dateString",
                    visitorCount: {
                        $sum: 1
                    }
                }
            }]).toArray()
            const invite = await _invite.getInvites(dateRange)
            if (!invite) {
                throw (new Error("Invite not Found"))
            } else {
                return {
                    message: "Success",
                    inviteCount: invite.data.length,
                    visitorCount: visitor.data.length,
                    visitorCountArray: visitorCheckedArray,
                    visitorCheckedInCount: visitorCheckedIn.length
                }
            }
        }
    }
}

module.exports = _dashboard;