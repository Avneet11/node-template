"use strict";

const Service = require("../Services");
const UniversalFunctions = require("../Utils/UniversalFunctions");
const CONFIG = require("../config");
const TokenManager = require("../Lib/TokenManager");
const NotificationModule = require("../Lib/NotificationModule");

const async = require("async");
const moment = require("moment");

//Company Registration


const countCompanies = function(userData, queryData, callback) {
  var noOfCompanies = null;
  var from = moment(queryData.from).toDate();
  var to = moment(queryData.to).toDate();
  var timing = queryData.timing;
  console.log("From: ", from);
  async.series(
    [
      function(cb) {
        var options = {
          lean: true
        };
        switch (timing) {
          case "hourly":
            var criteria = [
              {
                $match: {
                  registrationDate: {
                    $gte: moment()
                      .utc()
                      .subtract(1, "d")
                      .toDate(),
                    $lte: moment()
                      .utc()
                      .toDate()
                  }
                }
              },
              {
                $project: {
                  h: {
                    $hour: "$registrationDate"
                  }
                }
              },
              {
                $group: {
                  _id: {
                    hour: "$h"
                  },
                  count: { $sum: 1 }
                }
              },
              {
                $sort: {
                  "_id.hour": 1
                }
              }
            ];
            Service.SuperAdminService.getCount(criteria, function(err, data) {
              if (err) {
                cb(err);
              } else {
                noOfCompanies = data;
                cb();
              }
            });
            break;
          case "biweekly":
            var criteria = [
              {
                $match: {
                  registrationDate: {
                    $gte: moment()
                      .utc()
                      .subtract(15, "d")
                      .toDate(),
                    $lte: moment()
                      .utc()
                      .toDate()
                  }
                }
              },
              {
                $project: {
                  y: {
                    $year: "$registrationDate"
                  },
                  m: {
                    $month: "$registrationDate"
                  },
                  d: {
                    $dayOfMonth: "$registrationDate"
                  }
                }
              },
              {
                $group: {
                  _id: {
                    year: "$y",
                    month: "$m",
                    day: "$d"
                  },
                  count: { $sum: 1 }
                }
              },
              {
                $sort: {
                  "_id.year": 1,
                  "_id.month": 1,
                  "_id.day": 1
                }
              }
            ];
            Service.SuperAdminService.getCount(criteria, function(err, data) {
              if (err) {
                cb(err);
              } else {
                noOfCompanies = data;
                cb();
              }
            });
            break;
          case "monthly":
            var criteria = [
              {
                $match: {
                  registrationDate: {
                    $gte: moment()
                      .utc()
                      .subtract(30, "d")
                      .toDate(),
                    $lte: moment()
                      .utc()
                      .toDate()
                  }
                }
              },
              {
                $project: {
                  y: {
                    $year: "$registrationDate"
                  },
                  m: {
                    $month: "$registrationDate"
                  },
                  d: {
                    $dayOfMonth: "$registrationDate"
                  }
                }
              },
              {
                $group: {
                  _id: {
                    year: "$y",
                    month: "$m",
                    day: "$d"
                  },
                  count: { $sum: 1 }
                }
              },
              {
                $sort: {
                  "_id.year": 1,
                  "_id.month": 1,
                  "_id.day": 1
                }
              }
            ];
            Service.SuperAdminService.getCount(criteria, function(err, data) {
              if (err) {
                cb(err);
              } else {
                noOfCompanies = data;
                cb();
              }
            });
            break;
          case "yearly":
            var criteria = [
              {
                $match: {
                  registrationDate: {
                    $gte: moment()
                      .utc()
                      .subtract(365, "d")
                      .toDate(),
                    $lte: moment()
                      .utc()
                      .toDate()
                  }
                }
              },
              {
                $project: {
                  y: {
                    $year: "$registrationDate"
                  },
                  m: {
                    $month: "$registrationDate"
                  },
                  d: {
                    $dayOfMonth: "$registrationDate"
                  }
                }
              },
              {
                $group: {
                  _id: {
                    year: "$y",
                    month: "$m",
                    day: "$d"
                  },
                  count: { $sum: 1 }
                }
              },
              {
                $sort: {
                  "_id.year": 1,
                  "_id.month": 1,
                  "_id.day": 1
                }
              }
            ];
            Service.SuperAdminService.getCount(criteria, function(err, data) {
              if (err) {
                cb(err);
              } else {
                noOfCompanies = data;
                cb();
              }
            });
            break;
          case "custom":
            var criteria = [
              {
                $match: {
                  registrationDate: { $gte: from, $lte: to }
                }
              },
              {
                $project: {
                  y: {
                    $year: "$registrationDate"
                  },
                  m: {
                    $month: "$registrationDate"
                  },
                  d: {
                    $dayOfMonth: "$registrationDate"
                  }
                }
              },
              {
                $group: {
                  _id: {
                    year: "$y",
                    month: "$m",
                    day: "$d"
                  },
                  count: { $sum: 1 }
                }
              },
              {
                $sort: {
                  "_id.year": 1,
                  "_id.month": 1,
                  "_id.day": 1
                }
              }
            ];
            Service.SuperAdminService.getCount(criteria, function(err, data) {
              if (err) {
                cb(err);
              } else {
                noOfCompanies = data;
                cb();
              }
            });
            break;
          default:
            cb(CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
        }
      }
    ],
    function(err) {
      if (err) {
        callback(err);
      } else {
        callback(null, noOfCompanies);
      }
    }
  );
};

module.exports = {
  countCompanies
};
