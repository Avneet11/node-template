const express = require("express");
const router = express();
const dotenv = require("dotenv");
dotenv.config();

const userRoute = require("./components/user/userRoute");
const vendorRoute = require("./components/vendor/vendorRoute");
const bookingRoute = require("./components/booking/bookingRoute");

router.use(vendorRoute);
router.use(bookingRoute);
router.use(userRoute);

module.exports = router;
