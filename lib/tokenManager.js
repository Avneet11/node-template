const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
const _tokenManager = {};

_tokenManager.signToken = async function(data) {
    // Assign jwt token
    const secret = process.env.SECRET || "Development";
    const token = jwt.sign(data, secret, {
        expiresIn: "1d" // expires in 24 hours
    });
    if (!token) {
        throw new Error("Unable to sign token !!");
    } else {
        return (token);
    }
};

_tokenManager.verifyToken = async function(token) {
    // Assign jwt token
    const secret = process.env.SECRET || "Development";
    const data = await jwt.verify(token, secret)
    if (!data) {
        throw new Error("token not verified !!");
    } else {
        return (data);
    }
};

module.exports = _tokenManager;
