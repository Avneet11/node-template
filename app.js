"use strict";

/*
 * This file exports the app that is used by the server to expose the routes.
 * And make the routes visible.
 */

const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const routes = require("./routes");
const auth = require("./lib/auth");
const errorHandler = require("./utils/errorHandler.js");

// Express App
const app = express();
// Setup DB Connection
require("./db");

// Use default logger for now
app.use(logger("dev"));
app.use(cors());
app.use(bodyParser.json({ limit: '3mb', extended: false }));
app.use(bodyParser.urlencoded({ limit: '3mb', extended: false }));
app.use(cookieParser());

// This is to check if the service is online or not
app.use("/ping", function (req, res) {
    res.json({ reply: "pong" });
    res.end();
});

//app.use(auth.common);

// Mount the Routes
app.use("/v1", routes);

app.use(errorHandler);

// Export the express app instance
module.exports = app;
